//
//  ViewController.m
//  TYPhoneCodeButtonModule
//
//  Created by 夏伟 on 2016/10/20.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYPhoneCodeButton.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet TYPhoneCodeButton *verify_codeBtn;

@end

@implementation ViewController

- (IBAction)verifyBtnClicked:(id)sender {
    [self.verify_codeBtn startUpTimer];
}

@end

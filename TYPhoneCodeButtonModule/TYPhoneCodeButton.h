//
//  TYPhoneCodeButton.h
//  TYPhoneCodeButtonModule
//  验证码按钮
//  Created by 夏伟 on 16/9/22.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TYPhoneCodeButton : UIButton

// 开始倒计时
- (void)startUpTimer;
// 停止倒计时
- (void)invalidateTimer;

@end

//
//  TYPhoneCodeButton.m
//  TYPhoneCodeButtonModule
//
//  Created by 夏伟 on 16/9/22.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYPhoneCodeButton.h"
#import <UIColor-TYAdditions/UIColor+TYAdditions.h>

@interface TYPhoneCodeButton ()

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) NSTimeInterval durationToValidity;

@end

@implementation TYPhoneCodeButton

#pragma mark - Init
- (void)dealloc {
    [self invalidateTimer];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self commonInit];
    }
    return self;
}

#pragma mark - Views
- (void)commonInit {
    self.layer.borderColor = [UIColor add_colorWithRGBHexValue:0xc7c6cb].CGColor;
    self.layer.borderWidth = 1.0;

    self.titleLabel.font = [UIFont systemFontOfSize:13];
    self.enabled = YES;
}

#pragma mark - Override Methods
- (CGSize)intrinsicContentSize {
    return CGSizeMake(90, 25);
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];

    UIColor *foreColor = [UIColor add_colorWithRGBHexValue:enabled ? 0x464646 : 0xCCCCCC];
    [self setTitleColor:foreColor forState:UIControlStateNormal];
    if (enabled) {
        [self setTitle:@"获取验证码" forState:UIControlStateNormal];
    } else if ([self.titleLabel.text isEqualToString:@"获取验证码"]) {
        [self setTitle:@"正在发送..." forState:UIControlStateNormal];
    }
}

#pragma mark - Timer
- (void)startUpTimer {
    _durationToValidity = 60;

    if (self.isEnabled) {
        self.enabled = NO;
    }
    [self setTitle:[NSString stringWithFormat:@"%.0f 秒", _durationToValidity] forState:UIControlStateNormal];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(redrawTimer:)
                                                userInfo:nil
                                                 repeats:YES];
}

- (void)invalidateTimer {
    if (!self.isEnabled) {
        self.enabled = YES;
    }
    [self.timer invalidate];
    self.timer = nil;
}

- (void)redrawTimer:(NSTimer *)timer {
    _durationToValidity--;
    if (_durationToValidity > 0) {
        // 防止 button_title 闪烁
        self.titleLabel.text = [NSString stringWithFormat:@"%.0f 秒", _durationToValidity];
        [self setTitle:[NSString stringWithFormat:@"%.0f 秒", _durationToValidity] forState:UIControlStateNormal];
    } else {
        [self invalidateTimer];
    }
}

@end

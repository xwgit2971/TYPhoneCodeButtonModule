Pod::Spec.new do |s|
  s.name = 'TYPhoneCodeButtonModule'
  s.version = '0.0.2'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = '验证码按钮(业务公用组件)'
  s.homepage = 'https://gitlab.com/xwgit2971/TYPhoneCodeButtonModule'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYPhoneCodeButtonModule.git', :tag => s.version }
  s.source_files = 'TYPhoneCodeButtonModule/*.{h,m}'
  s.framework = 'Foundation', 'UIKit'
  s.requires_arc = true
  s.dependency  "UIColor-TYAdditions"
end
